/* LICENSE
 *
 * Copyright (c) 2013 xEvilReeperx
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include <boost/polygon/polygon.hpp>
#include <vector>
#include <memory>
#include <SDL_types.h>
#include "PolygonGeometry.h"
#include "UIWindow.h"
#include "UIScrollbar.h"

using UI::UIScrollbar_t;
using UI::BasicUIScrollbarPtr;

class App {
	// local typedefs
	typedef std::shared_ptr<PolygonGeometry>			PolygonGeometryPtr;


	// local variables
		// SDL and rendering related
		struct SDL_Surface*				m_screen;			///< Render surface

		bool							m_bShowSegments;
		bool							m_bShowVertices;	

		// interface-related
		WindowPtr						m_rootWindow;		///< Root window
		WindowPtr						m_renderWindow;		///< invisible render window; we use its bounds as a clipping rect
		
		BasicUIScrollbarPtr				m_renderVScroll;	///< for render window
		BasicUIScrollbarPtr				m_renderHScroll;	///< for render window


		// Data
		PolygonGeometryPtr				m_polygonGeometry;	///< Geometry represented purely by n-sided polygons


	// private functions
	int		Initialize();								///< Initialize SDL and other objects
	void	Unload();									///< Unload SDL
	bool	HandleEvents();								///< handle incoming key, mouse etc events.  Returns true if
														///< app should continue
	void	Render();									///< Render ui and active data model

		// subfunctions
		void	InitUI();								///< Create UserInterface, set up UI elements and callbacks
		void	InitGeometry();							///< Create data objects which represent geometry in various forms

	
	//void	Triangulate();								///< perform triangulation

public:
	// main functions
	App();												///< Constructor
	~App();												///< Destructor

	void	Go();										///< Mainloop of the program
	void	Screenshot();								///< Take a screenshot
	//bool	EventCallback(const SDL_Event& evt);		///< for unhandled SDL events we might be interested in

	// callbacks
	void	DoNothing(int i) { (i); }
};

