#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>
#include <stack>

// Originator = object which creates mementoes
// StateHolder = some custom class which can contain Originator's state (or enough information to reconstruct it)
template <class Originator, class StateHolder>
class Memento : public boost::noncopyable { 
public:
	virtual ~Memento() { if (_state) delete _state; }

	// these aren't supported by vs2010 apparently
	//Memento&	operator=(const Memento& other) = delete;
	//Memento(const Memento& other) = delete;

private:
	friend Originator;

	Memento(StateHolder* state = nullptr) : _state(state) {}

	// make these virtual so derived classes can implement them and still let
	// our friend Originator access that derived state info
	virtual void	SetState(StateHolder* state) { 
		if (_state)
			delete _state; // delete existing state
		_state = state; 
	}

	virtual const StateHolder*	GetState() const { return _state; }

private:
	StateHolder*			_state;
};


// interface for objects which can create mementos
	// forward declaration needed for Caretaker ..
	template <class MemType>
	class MementoCaretaker;

template <class Originator, class StateHolder>
struct MementoOriginator {
	typedef Memento<Originator, StateHolder>		MementoType;
	typedef MementoCaretaker<MementoType>			MementoCaretakerType;	///< Just to add a little convenience for classes which will hold an Originator's mementoes externally

	virtual MementoType*	CreateMemento() const = 0;				///< Originators implement this function to create a snapshot of their current state, which can be restored later
	virtual void	SetMemento(MementoType* memento) = 0;			///< Originators implement this function to restore their state
};


/**
	Define a caretaker class that will store mementoes of the given type for
	us.  We can commit new mementoes to the list or retrieve them in sequential order
**/
template <class MementoType>
class MementoCaretaker : public boost::noncopyable {
	typedef boost::shared_ptr<MementoType>	MementoPtr;
	typedef std::stack<MementoPtr>			MementoStack;

	MementoStack						m_pastOperations;		///< Our stack of stored mementoes.  The top of this stack serves as the "current" memento
	MementoStack						m_futureOperations;		///< Stack of future (redo) operations.  If this stack contains any entries, they are wiped out when Commit() is called

	MementoType*		GetCurrentMemento() const {
		// return the "current" memento (top of past stack)
		if (!m_pastOperations.empty()) {
			return m_pastOperations.top().get();
		} else if (!m_futureOperations.empty()) { // if the user has used up all the undo operations, give them the absolute oldest memento we can
			return m_futureOperations.top().get();
		} else {
			// oops, error.  
			throw std::exception("No mementoes available; did you forget to have the originator create a memento for its initial state?");
		}
	}

public:

	MementoType*		Forward() {									///< "REDO" Advance history by one (if possible) and return that state
		// move one future operation to the past stack
		if (!m_futureOperations.empty()) {
			m_pastOperations.push(m_futureOperations.top());
			m_futureOperations.pop();
		}

		// return the "current" memento
		return GetCurrentMemento();
	}

	MementoType*		Backward() {								///< "UNDO" Rewind history by one (if possible) "" "" " 
		// move one past operation to the future stack
		if (!m_pastOperations.empty()) {
			m_futureOperations.push(m_pastOperations.top());
			m_pastOperations.pop();
		}

		// return the "current" memento
		return GetCurrentMemento();
	}

	// note: returns number of mementoes erased
	unsigned int	Commit(MementoType* memento) {				///< Puts given memento into our possession.  All "future" operations of redo are erased permanently
		unsigned int counter = 0;

		// erase all future operations
		while (!m_futureOperations.empty())	{
			m_futureOperations.pop();
			++counter;
		};

		// slap the new memento onto our stack
		m_pastOperations.push(MementoPtr(memento));

		// return # of mementoes freed
		return counter;
	}


	bool	CanRedo() const {
		return (!m_futureOperations.empty());
	}

	bool	CanUndo() const {
		return (!m_pastOperations.empty());
	}

	// Delete ALL stored mementoes; return # destroyed
	unsigned int	EraseAll() {
		unsigned int	counter = 0;

		while (!m_futureOperations.empty()) {
			++counter;
			m_futureOperations.pop(); // shared_ptr will clean up the memory for us
		};

		while (!m_pastOperations.empty()) {
			++counter;
			m_pastOperations.pop();
		};

		return counter;
	}
};