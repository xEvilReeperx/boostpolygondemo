#pragma once
#include "UICheckbox.h"
#include <boost/shared_ptr.hpp>
#include <vector>
#include <boost/foreach.hpp>



/** Option is essentially a checkbox that exists in a group of mutually
	exclusive items **/
class UIOption : public UICheckbox {

public:
	typedef std::vector<UIOption*>				OptionVector;
	typedef boost::shared_ptr<OptionVector>		OptionGroup;				///< this construct is what will allow us to keep a "group" of option buttons together

	UIOption(const std::string& label, const SDL_Rect& boundaries, CallbackFunctionType cb, OptionGroup opt = OptionGroup(), Uint32 flags = UISTYLE::NONE) :
		UICheckbox(label, boundaries, cb, flags), m_group(opt) {
		
		// is our group valid?  if not, we'll start one
		if (!m_group)
			m_group = OptionGroup(new OptionVector);

		// remember that we're sharing this group, so it's our job to let
		// the others know that we're in the party too now
		//
		// Although it'd be nice to make this a bit safer to use, I'm going the
		// easy route with raw pointers simply because it's reasonable to assume
		// that UIOptions that exist in a group will all be destroyed at the same
		// time
			m_group->push_back(this);
	}

	UIOption(const std::string& label, const SDL_Rect& boundaries, CallbackFunctionType cb, Uint32 flags) :
		UICheckbox(label, boundaries, cb, flags) {
			m_group = OptionGroup(new OptionVector);
			m_group->push_back(this);
	}

	virtual ~UIOption() {
		// find ourself in our option group, if one exists
		if (!m_group->empty()) {
			OptionVector::iterator me = std::find(m_group->begin(), m_group->end(), this);

			if (me != m_group->end()) 
				m_group->erase(me);
		}
	}

	virtual OptionGroup	GetOptionGroup() const { return m_group; }

	virtual void	Render(SDL_Surface* screen) const override {
		SDL_Rect		rect = m_bounds;

		// small circle on the left
			// outer circle (backcolor)
				rect.w = m_bounds.h;		// square rect

				circleColor(screen, rect.x + rect.w / 2, rect.y + rect.h / 2, static_cast<Sint16>(rect.w / 2 * 0.8f), GetBackgroundColor().GetGfxColor());

			// selected?
			if (m_checked) {
				// filled in circle (foreground color)
				Sint16	rad = std::max<Sint16>(static_cast<Sint16>(rect.w / 2 * 0.8 - 4), 3);

				filledCircleColor(screen, rect.x + rect.w / 2, rect.y + rect.h / 2, rad, GetForegroundColor().GetGfxColor());
			}

		// text, just to the right of the optionbox option
		rect.x = rect.x + rect.w + 5;
		
		RenderText(screen, m_label, GetForegroundColor(), rect.x, m_bounds.y + m_bounds.h / 2 - 4);
	}

	// notice the lack of an inverted value compared to UICheckbox - Trigger is back to assuming a click == selected
	virtual void	SetValue(bool bValue) { m_checked = bValue; Trigger(m_checked ? 1 : 0); }

protected:
	OptionGroup					m_group;									///< group this button belongs to (if any)

	virtual void Trigger(int arg) {
		if (arg == 1) { // we've been selected!  Deselect all our peers, wouldn't want to share the glory
			m_checked = true;

			// only if we're actually in a valid group, of course
			if (m_group)
				BOOST_FOREACH(UIOption* p, *m_group) {
					if (p == this)
						continue;

					p->SetValue(false);		// this will trigger callbacks for all of the false checkboxes first
				}

			UIButton::Trigger(1); // now fire off our event; we already know we've just been selected
		}
	}

};