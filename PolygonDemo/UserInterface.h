#pragma once
#include "UIWindow.h"
#include "UIButton.h"
#include "UICheckbox.h"
#include "UIStaticText.h"
#include "UIStaticFrame.h"
#include "UIOption.h"
#include "UIScrollbar.h"
#include <vector>
#include <list>
#include <memory>
#include <functional>
#include <boost/foreach.hpp>
#include <SDL_video.h>

#undef CreateWindow			// removed to avoid collision

class UserInterface {
	typedef std::shared_ptr<UIWindow>		WindowPtr;
	typedef std::vector<WindowPtr>			WindowVector;

	typedef std::function<bool (const SDL_Event&)>				CallbackFunctionType;
	typedef std::list<CallbackFunctionType>						CallbackList;


	// every UI item is a window of some kind in the end
	WindowVector							m_uiWindows;

	// listeners for unhandled events
	CallbackList							m_eventListeners;


public:
	void	DestroyAllWindows() {
		m_uiWindows.clear();
	}


	void	AddWindow(WindowPtr uiWindow) {
		if (uiWindow)
			m_uiWindows.push_back(uiWindow);
	}


	// add a window -- really just a blank rectangle that doesn't respond to user input at all
	void	CreateWindow(const SDL_Rect& boundaries, const GfxColor& color) {
		AddWindow(std::make_shared<UIWindow>(boundaries, color));
	}


	// create a button
	void	CreateButton(const std::string& label, const SDL_Rect& boundaries, UIButton::CallbackFunctionType f) {
		AddWindow(std::make_shared<UIButton>(label, boundaries, f));
	}


	// create a checkbox
	UICheckbox&	CreateCheckbox(const std::string& label, const SDL_Rect& boundaries, UICheckbox::CallbackFunctionType f) {
		UICheckbox*	cb = new UICheckbox(label, boundaries, f);
		AddWindow(WindowPtr(cb));

		return *cb;
	}


	// create an option button
	UIOption&	CreateOption(const std::string& label, const SDL_Rect& boundaries, UIOption::CallbackFunctionType f, UIOption::OptionGroup g = UIOption::OptionGroup()) {
		UIOption* opt = new UIOption(label, boundaries, f, g);
		AddWindow(WindowPtr(opt));

		return *opt;
	}


	// create static label
	void	CreateStaticText(const std::string& label, const SDL_Rect& boundaries) {
		AddWindow(WindowPtr(new UIStaticText(label, boundaries)));
	}


	// create a static frame
	void	CreateStaticFrame(const std::string& label, const SDL_Rect& boundaries, const GfxColor& color = GFX_LIGHT_GREY) {
		AddWindow(WindowPtr(new UIStaticFrame(label, boundaries, color)));
	}


	// create a scrollbar
	template <class T>
	UIScrollbar<T>&	CreateScrollbar(const SDL_Rect& boundaries, typename UIScrollbar<T>::SCROLL_DIRECTION type, const GfxColor& bkgColor, const GfxColor& btnDarkColor, const GfxColor& btnLitColor, T minValue, T maxValue, T startValue, T bigStep, T interval) {
		UIScrollbar<T>*	sb = new UIScrollbar<T>(boundaries, type, bkgColor, btnDarkColor, btnLitColor, minValue, maxValue, startValue, bigStep, interval);

		AddWindow(WindowPtr(sb));

		return *sb;
	}


	// Render all elements
	void	Render(SDL_Surface* screen) {
		BOOST_FOREACH(const WindowPtr& uiElement, m_uiWindows) {
			uiElement->Render(screen);
		}
	}


	/// Handles input events.  Returns true so long as the program should continue running
	bool	HandleEvents() {
		SDL_Event		evt;

		while (SDL_PollEvent(&evt)) {
			if (evt.type == SDL_QUIT) {
				return false;
			} else if (evt.type == SDL_KEYDOWN)
				if (evt.key.keysym.sym == SDLK_ESCAPE)
					return false;

			bool	handled = false;

			BOOST_FOREACH(const WindowPtr& uiElement, m_uiWindows) {
				if (uiElement->HandleEvent(evt)) {
					handled = true;

					if (!uiElement->m_eventCallback._Empty())
						uiElement->m_eventCallback(evt);

					break;			// an element handled this particular event; don't let any others process it
				}
			}

			if (!handled) {
				// no UI windows processed the event (at least, not in a manner that
				// consumed it).
				//
				// let regular event listeners have a turn:

				BOOST_FOREACH(CallbackFunctionType f, m_eventListeners) {
					if (f(evt)) 
						break; // event was handled
				}
			}
		};

		return true;
	}


	void	RegisterEventListener(CallbackFunctionType f) {
		if (!f._Empty())
			m_eventListeners.push_back(f);
		
		// why no removal?  Because std::function doesn't
		// have a valid comparison operator (due to technical limitations)
	}
};