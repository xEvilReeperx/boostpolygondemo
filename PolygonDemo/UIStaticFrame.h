#pragma once
#include "UIWindow.h"
#include "UIStaticText.h"
#include <string>

/** Just an enclosing black frame with a text header **/
class UIStaticFrame : public UIWindow {
	UIStaticText				m_text;

public:
	UIStaticFrame(const std::string& title, const SDL_Rect& boundaries, const GfxColor& color, Uint32 flags = UISTYLE::NONE) : 
		UIWindow(boundaries, color, flags), 
		m_text(title, boundaries) {
	
		m_text.SetBackgroundColor(GFX_INVISIBLE);
		m_text.SetBounds(CalculateTextPosition());
	}

	UIStaticText&	GetText() { return m_text; } ///< In case user wants to modify text colors etc

	SDL_Rect	CalculateTextPosition() {
		SDL_Rect		pos = {m_bounds.x + 4, m_bounds.y - 4, m_bounds.w, 8};
		pos.w = std::min<Sint16>(m_bounds.w, 8 * static_cast<Sint16>(m_text.GetLabel().size() + 1));

		return pos;
	}

	void	Render(SDL_Surface* screen) const override {
		UIWindow::Render(screen);		// background color

		// enclosing frame
			rectangleColor(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, 0x000000FF);

			// erase part of the frame that would obscure text
				lineColor(screen, m_text.GetBounds().x - 2, m_text.GetBounds().y, std::min<Sint16>(m_text.GetBounds().x + m_text.GetBounds().w, m_bounds.x + m_bounds.w), m_bounds.y, GetBackgroundColor().GetGfxColor());

				if (HasStyle(UISTYLE::FANCY))
					boxColor(screen, m_text.GetBounds().x - 2, m_text.GetBounds().y - 2, std::min<Sint16>(m_text.GetBounds().x + m_text.GetBounds().w, m_bounds.x + m_bounds.w), m_bounds.y, GetBackgroundColor().GetGfxColor());
	
			m_text.Render(screen);

		if (HasStyle(UISTYLE::FANCY)) {
			// finish off the fancy effect by including the "tab" created earlier in the border
				// left side
				lineColor(screen, m_text.GetBounds().x - 2, m_bounds.y, m_text.GetBounds().x - 2, m_text.GetBounds().y - 2, 0x000000FF);
	
				// right side
				lineColor(screen, std::min<Sint16>(m_text.GetBounds().x + m_text.GetBounds().w, m_bounds.x + m_bounds.w), m_bounds.y, std::min<Sint16>(m_text.GetBounds().x + m_text.GetBounds().w, m_bounds.x + m_bounds.w), m_text.GetBounds().y - 2, 0x000000FF);

				// top line
				lineColor(screen, m_text.GetBounds().x - 2, m_text.GetBounds().y - 2, std::min<Sint16>(m_text.GetBounds().x + m_text.GetBounds().w, m_bounds.x + m_bounds.w), m_text.GetBounds().y - 2, 0x000000FF);
		}
	}
};