#pragma once
#include <SDL_events.h>
#include <SDL_video.h>
#include <SDL_gfxPrimitives.h>
#include <cassert>
#include <list>
#include <functional>
#include <type_traits>
#include "GfxColor.h"
#include "UIStyles.h"

typedef std::shared_ptr<class UIWindow>		WindowPtr;

using UI::UISTYLE;


// basic abstract interface for UI elements; provides some
// convenience items (bounds and PointWithinBounds)
class UIWindow {
public:
	typedef std::function<void (const SDL_Event&)>		EventInterceptionCallbackType;
	typedef std::list<WindowPtr>						ChildList;

	friend class UserInterface;

protected:

	ChildList							m_children;
	UIWindow*							m_parent;

	SDL_Rect							m_bounds;
	GfxColor							m_backColor;					// background color of this window
	GfxColor							m_foreColor;					// forecolor of the window .. for use by derived classes
	Uint32								m_style;

	EventInterceptionCallbackType		m_eventCallback;			// if managed by a UserInterface class, this
																	// callback will be used whenever the derived class
																	// returns True in HandleEvent (indicating that
																	// it has intercepted and consumed an event)

	/// helper function, kept here since it's bound to be used pretty often in derived classes
	static bool	IsPointWithinBounds(int x, int y, const SDL_Rect& bounds) {
		if (x >= bounds.x)
			if (x < bounds.x + bounds.w)
				if (y >= bounds.y)
					if (y < bounds.y + bounds.h)
						return true;

		return false;
	}


public:
	UIWindow(const SDL_Rect& boundaries, const GfxColor& color = GFX_LIGHT_GREY, Uint32 flags = UISTYLE::NONE) : 
		m_bounds(boundaries), 
		m_backColor(color),
		m_parent(nullptr),
		m_style(flags) {

			assert(m_bounds.w > 0);
			assert(m_bounds.h > 0);
			m_foreColor = GFX_BLACK;
	}

	UIWindow(const SDL_Rect& boundaries, Uint32 flags = UISTYLE::NONE) :
		m_bounds(boundaries),
		m_style(flags),
		m_parent(nullptr),
		m_backColor(GFX_LIGHT_GREY) {

			assert(m_bounds.w > 0);
			assert(m_bounds.h > 0);
			m_foreColor = GFX_BLACK;
	}

	virtual ~UIWindow() {}

	virtual void	Render(SDL_Surface* screen) const {
		if (m_backColor.a > 0) {
			boxColor(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, m_backColor.GetGfxColor());
		
			// outline
			rectangleColor(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, 0x000000FF);
		}

		// render all children
		for (auto& child : m_children)
			child->Render(screen);
	}

	virtual bool	HandleEvent(const SDL_Event& evt) { 
		// run event through all children
		for (auto& child : m_children) {
			if (child->HandleEvent(evt)) {
				// if the child has a function to be called when it intercepts
				// an event, trigger it now
				if (!child->m_eventCallback._Empty())
					child->m_eventCallback(evt);

				return true; // event handled; don't let any other children handle it
			}
		}
	
		return false;
	}

	bool	HasChild(const WindowPtr& ptr) const {
		for (auto& child : m_children)
			if (child == ptr)
				return true;
		return false;
	}

	void	AddChild(WindowPtr ptr) { // in this case, taking by value is actually an optimization (if the child is inserted)
		if (!HasChild(ptr)) {
			assert(ptr->m_parent == nullptr);
			ptr->m_parent = this;
			m_children.emplace_back(ptr);
		}
	}

	bool	RemoveChild(WindowPtr ptr) {
		auto foundIt = std::find(m_children.begin(), m_children.end(), ptr);
		if (foundIt != m_children.end()) {
			(*foundIt)->m_parent = nullptr;
			m_children.erase(foundIt);
		}

		return (foundIt != m_children.end());
	}

	unsigned int	GetChildCount() const {
		return m_children.size();
	}

	unsigned int	GetSiblingCount() const {
		if (!m_parent)
			return 0; // no siblings

		return m_parent->m_children.size() - 1;
	}

	UIWindow*	GetParent() const { return m_parent; }
	Uint32		GetStyle() const { return m_style; }
	bool		HasStyle(Uint32 flag = 0) const {
		if (flag == 0) 
			return m_style > 0; // user wants to know if the window has any style paramters at all
		return (m_style & flag) != 0;
	}

	virtual void	SetBounds(const SDL_Rect& newBounds) { m_bounds = newBounds; }
	virtual void	AdjustBounds(const SDL_Rect& newBounds) { SetBounds(newBounds);	}
	virtual SDL_Rect	GetBounds() const { return m_bounds; }
	virtual GfxColor	GetBackgroundColor() const { return m_backColor; }
	virtual GfxColor	GetForegroundColor() const { return m_foreColor; }
	virtual void		SetBackgroundColor(const GfxColor& color) { m_backColor = color; }
	virtual void		SetForegroundColor(const GfxColor& color) { m_foreColor = color; }
	virtual void		SetStyle(Uint32 style) { m_style = style; }
	virtual void		AddStyle(Uint32 flag) { m_style |= flag; }

	// note: this only works when the "target" control is being managed by
	// a UserInterface object
	void	SetCallbackWhenEventHandled(EventInterceptionCallbackType f) {
		if (!f._Empty())
			m_eventCallback = f;
	}
};


namespace UIHelper {
	template <class Type>
	inline Type*	AsType(WindowPtr& ptr) {
		static_assert(std::is_polymorphic<Type>::value, "not a polymorphic type");
		static_assert(std::is_convertible<Type, UIWindow>::value, "types incompatible");
		assert(ptr.use_count());

		return dynamic_cast<Type*>(ptr.get());
	}
}