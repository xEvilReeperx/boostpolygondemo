#pragma once
#include "UIWindow.h"
#include <SDL_gfxPrimitives.h>
#include <SDL_gfxPrimitives_font.h>
#include <cassert>
#include <functional>
//#include <boost/function.hpp>
#include <string>
#ifdef WIN32
#include <Windows.h>
#endif

///
class UIButton : public UIWindow {
public:
	enum BUTTON_STATE {
		NOT_PRESSED = 0,
		PRESSED = 1
	};

	//typedef boost::function<void (int /* 0 = not pressed, 1 = pressed */)>		CallbackFunctionType;
	typedef std::function<void (int /* 0 = not pressed, 1 = pressed */)>		CallbackFunctionType;

	// constructor & destructor
	UIButton(const std::string& lbl, const SDL_Rect& boundaries, CallbackFunctionType f, Uint32 flags = UISTYLE::NONE) : UIWindow(boundaries, flags), 
																	 m_label(lbl), 
																	 m_callback(f),
																	 m_buttonState(NOT_PRESSED) {
		SetBackgroundColor(GFX_BLACK);
		SetForegroundColor(GFX_WHITE);
	}

	virtual ~UIButton() {}


	// check to see if we were clicked
	virtual bool	HandleEvent(const SDL_Event& evt) override {
		if (UIWindow::HandleEvent(evt))
			return true;

		// if (left click down)
		//		within our boundaries?
		//			change state to pressed but don't trigger event (user can move mouse off button if they clicked accidentally, as long as the mouse isn't released
		//			return true
		//
		// if (left click up)
		//		within our boundaries?
		//			if (state = pressed)
		//				trigger callback
		//		else not within our boundaries
		//		change state to not pressed
		
		switch (evt.type) {
			case SDL_MOUSEBUTTONDOWN:
				if (evt.button.button == SDL_BUTTON_LEFT)
					if (IsPointWithinBounds(evt.motion.x, evt.motion.y, m_bounds)) {
						m_buttonState = PRESSED;
						return true;		// handled the event
					}

				break;

			case SDL_MOUSEBUTTONUP:
				if (evt.button.button == SDL_BUTTON_LEFT) 
					if (IsPointWithinBounds(evt.motion.x, evt.motion.y, m_bounds)) {
						if (m_buttonState == PRESSED) {
							Trigger(PRESSED);
							m_buttonState = NOT_PRESSED;
							return false;	// don't capture mouseup events for ourselves; if another control
											// was clicked in and the mouse moved out, it's still going to need
											// to know about that mouseup 
						}
					} else m_buttonState = NOT_PRESSED; // left mouse released but not over us; don't trigger

				break;
		} // end evt.type switch

		return false; // didn't handle event
	}
	

	// render function
	virtual void		Render(SDL_Surface* screen) const override {
		// naive string location, assuming characters are 8x8 pixels
		Sint16		xString = static_cast<Sint16>(m_bounds.x + m_bounds.w / 2 - m_label.size() * 4); // (simplified / 2 * 8)
		Sint16		yString = static_cast<Sint16>(m_bounds.y + m_bounds.h / 2 - 4);

		// create button colors
		GfxColor	outerColor = GetBackgroundColor();
		GfxColor	innerColor = GetForegroundColor();
	
		if (m_buttonState == PRESSED)
			std::swap(innerColor, outerColor);

		// outer rectangle
		if (HasStyle(UISTYLE::FANCY)) {
			roundedBoxColor(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, (m_bounds.w + m_bounds.h) / 2 / 8, outerColor.GetGfxColor());
		} else boxColor(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, outerColor.GetGfxColor());

		// render text
		if (!m_label.empty())
			RenderText(screen, m_label, innerColor, xString, yString);
	}

	virtual void SetState(BUTTON_STATE s) { m_buttonState = s; }
	virtual void SetLabel(const std::string& label) { m_label = label; }

	virtual BUTTON_STATE	GetState() const { return m_buttonState; }
	virtual const std::string	GetLabel() const { return m_label; }

protected:
	CallbackFunctionType	m_callback;						///< Callback which we trigger on a button press
	BUTTON_STATE			m_buttonState;
	std::string				m_label;						///< whatever label that goes on this button

	// Triggers callback
	virtual void	Trigger(int arg) {
		m_callback(arg);
	}

	// renders text; broken out of render function itself because other derived classes might want to move the text
	virtual void	RenderText(SDL_Surface* screen, const std::string& text, const GfxColor& color, Sint16 stringx, Sint16 stringy) const {
		// render text
		stringColor(screen, stringx, stringy, text.c_str(), color.GetGfxColor());
	}
};