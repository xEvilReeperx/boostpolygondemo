#pragma once
#include "PolygonTypes.h"

// forward decs
struct SDL_Surface;

using PolygonTypes::IntVector;

int hlineColorStore(SDL_Surface * dst, Sint16 x1, Sint16 x2, Sint16 y, Uint32 color);					///< Copy horizontal color to dst, no blending
int	filledPolygonColorStore(SDL_Surface* dst, const Sint16* vx, const Sint16* vy, int n, Uint32 color); ///< Copy color into polygon shape, no blending

void	RenderLineSegments(SDL_Surface* dest, const IntVector& vx, const IntVector& vy, Uint32 color);	///< Draws line segments of polygon or triangles
void	RenderVertices(SDL_Surface* dest, const IntVector& vx, const IntVector& vy, Uint32 color, int radius); ///< Draws vertices of polygons or triangles
