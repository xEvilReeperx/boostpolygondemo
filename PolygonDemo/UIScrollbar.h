#pragma once
#include "UIWindow.h"
#include "UIButton.h"
#include <memory>
#include <array>


template <class ValueType = int>
class UIScrollbar : public UIWindow {
	struct point {
		int x,y;
	};

	typedef std::shared_ptr<UIButton>		ButtonPtr;
	typedef std::array<point, 3>			TriangleArray;

	// button values
	ButtonPtr			m_decrease;					// We'll have our own little buttons that UserInterface isn't in control of
	ButtonPtr			m_increase; 
	ButtonPtr			m_slider;

	// generic color and render values
	GfxColor			m_btnDarkColor;				// color of the buttons when not being clicked or dragged
	GfxColor			m_btnLitColor;				// color of the buttons when clicked or dragged
	
	TriangleArray		m_decreaseTriangle;
	TriangleArray		m_increaseTriangle;

	// slider values
	SDL_Rect			m_mouseSliderOffset;		// used to calculate relative position of mouse on the slider button
	bool				m_sliderDragging;			// flag set when the slider is first being dragged, to let relevant
													// functions know the sliding offset is done


	// contents of the scrollbar
	ValueType			m_valueMax;
	ValueType			m_valueMin;
	ValueType			m_value;
	ValueType			m_bigStep;					// how much is added or removed from value when a click on the slider between buttons occurs
	ValueType			m_sliderInterval;			// how much of the range between min and max does the slider represent?  This
													// will chance the size of the button.  For instance, if min = 0 and max = 100 and
													// sliderInterval = 100, then the button's width would make up the entirety of the scroll
													// area of the scrollbar and could not move

	inline point	rotate90(const point& pointToRotate, const point& origin = {0, 0}) {
		point		retPoint;
		point		tempPoint;

		tempPoint.x = pointToRotate.x - origin.x;
		tempPoint.y = pointToRotate.y - origin.y;

		retPoint.x = -tempPoint.y + origin.y;
		retPoint.y = tempPoint.x + origin.x;

		return retPoint;
	}

	inline void		rotateTriangle90(TriangleArray& tris, const point& origin = {0, 0}) {
		for (auto& pt : tris)
			pt = rotate90(pt, origin);
	}


	// determine the position of the slider based on valueMin, valueMax and value
	// note: assumes these are all valid values to begin with
	SDL_Rect			CalculateSliderBounds() const {
		// we do know the size of the slider already ..
		SDL_Rect		sliderBounds = {0, 0, IsHorizontal() ? static_cast<Sint16>(m_sliderInterval / static_cast<float>(m_valueMax - m_valueMin) * (m_bounds.w - m_decrease->GetBounds().w - m_increase->GetBounds().w)) : m_bounds.w,
										      IsHorizontal() ? m_bounds.h : static_cast<Sint16>(m_sliderInterval / static_cast<float>(m_valueMax - m_valueMin) * (m_bounds.h - m_decrease->GetBounds().h - m_increase->GetBounds().h))};

		// now we merely determine position
		float	positionFactor = (m_value - m_valueMin) / static_cast<float>(m_valueMax - m_valueMin);
			// this gives us a scale 0.0 - 1.0 of where we are

		positionFactor = std::max<float>(0.0f, std::min<float>(1.0f, positionFactor)); // clamp positionFactor to [0.0, 1.0] 

		// important note: don't forget that the slider doesn't move across the entire window;
		// two pieces are reserved for the increase and decrease buttons!  We must account for them
		if (IsHorizontal()) {
			sliderBounds.y = m_bounds.y;

			// note: this represents the CENTER of the button
			sliderBounds.x = m_bounds.x + static_cast<Sint16>((m_bounds.w - m_decrease->GetBounds().w - m_increase->GetBounds().w - std::max<ValueType>(m_decrease->GetBounds().w, sliderBounds.w)) * positionFactor /* position */) + m_decrease->GetBounds().w /* offset from left edge */;

			// now we clamp the button into its maximum range: as it has a 
			// minimum size, the final few values on the edge of the scroll bar
			// are actually "partial" buttons: if the button was exactly accurate,
			// it would actually overlap the decrease and increase buttons by half its size!
			sliderBounds.x = std::max<Sint16>(m_bounds.x + m_decrease->GetBounds().w, std::min<Sint16>(sliderBounds.x /* value */, m_bounds.x + (m_bounds.w - m_decrease->GetBounds().w - sliderBounds.w) ));

		} else {
			sliderBounds.x = m_bounds.x;

			sliderBounds.y = m_bounds.y + static_cast<Sint16>((m_bounds.h - m_decrease->GetBounds().h - m_increase->GetBounds().h - std::max<ValueType>(m_decrease->GetBounds().h, sliderBounds.h)) * positionFactor) + m_decrease->GetBounds().h;
			sliderBounds.y = std::max<Sint16>(m_bounds.y + m_decrease->GetBounds().h, std::min<Sint16>(sliderBounds.y, m_bounds.y + (m_bounds.h - m_decrease->GetBounds().h - sliderBounds.h)));

		}
		
		return sliderBounds;
	}

public:
	UIScrollbar(const SDL_Rect& boundaries, Uint32 flags, const GfxColor& bkgColor, const GfxColor& btnDarkColor, const GfxColor& btnLitColor, ValueType minValue, ValueType maxValue, ValueType startValue, ValueType bigStep, ValueType interval) 
		: UIWindow(boundaries, bkgColor, flags),
			m_btnDarkColor(btnDarkColor),
			m_btnLitColor(btnLitColor),
			m_valueMin(minValue),
			m_valueMax(maxValue),
			m_value(startValue),
			m_bigStep(bigStep),
			m_sliderInterval(interval),
			m_sliderDragging(false) {

		// sanity checks
		assert(boundaries.w > 0);
		assert(boundaries.h > 0);
		assert(IsHorizontal() || IsVertical());

		// create directional buttons
		SDL_Rect	buttonSize = {0, 0,
									IsHorizontal() ? boundaries.h : boundaries.w, 
									IsHorizontal() ? boundaries.h : boundaries.w };

			// "up" triangle polygon
			point			originPt = {buttonSize.w / 2, buttonSize.h / 2};
			TriangleArray	upTri = {{
										{buttonSize.w / 2, 2},
										{2, buttonSize.h - 2},
										{buttonSize.w - 2, buttonSize.h - 2}
									}};


		SDL_Rect	decreaseRect;
		SDL_Rect	increaseRect;

		decreaseRect = increaseRect = buttonSize;

		if (IsHorizontal()) {
			// decrease button
			decreaseRect.x = m_bounds.x;
			decreaseRect.y = m_bounds.y;

			m_decreaseTriangle = upTri; // should be rotated cw 270deg
			rotateTriangle90(m_decreaseTriangle, originPt);
			rotateTriangle90(m_decreaseTriangle, originPt);
			rotateTriangle90(m_decreaseTriangle, originPt);

				for (auto& pt : m_decreaseTriangle) {
					pt.x += decreaseRect.x;
					pt.y += decreaseRect.y;
				}

			// increase button
			increaseRect.x = m_bounds.x + m_bounds.w - increaseRect.w;
			increaseRect.y = m_bounds.y;

			m_increaseTriangle = upTri; // rotate 90d cw
			rotateTriangle90(m_increaseTriangle, originPt);

				for (auto& pt : m_increaseTriangle) {
					pt.x += increaseRect.x;
					pt.y += increaseRect.y;
				}

		} else {
			decreaseRect.x = m_bounds.x;
			decreaseRect.y = m_bounds.y;

			m_decreaseTriangle = upTri; // no rotation

				for (auto& pt : m_decreaseTriangle) {
					pt.x += decreaseRect.x;
					pt.y += decreaseRect.y;
				}

			increaseRect.x = m_bounds.x;
			increaseRect.y = m_bounds.y + m_bounds.h - increaseRect.h;

			m_increaseTriangle = upTri; // rotate 180d cw
			rotateTriangle90(m_increaseTriangle, originPt);
			rotateTriangle90(m_increaseTriangle, originPt);

				for (auto& pt : m_increaseTriangle) {
					pt.x += increaseRect.x;
					pt.y += increaseRect.y;
				}
		}

		m_decrease = std::make_shared<ButtonPtr::element_type>("", decreaseRect, std::bind(&UIScrollbar::DecreaseButton, this, std::placeholders::_1));
		m_increase = std::make_shared<ButtonPtr::element_type>("", increaseRect, std::bind(&UIScrollbar::IncreaseButton, this, std::placeholders::_1));

		// slider
			// we need to do a few calcs first
			if (m_valueMin > m_valueMax) std::swap(m_valueMin, m_valueMax);
			if (m_value < m_valueMin) m_value = m_valueMin;

		m_slider = std::make_shared<ButtonPtr::element_type>("", CalculateSliderBounds(), std::bind(&UIScrollbar::SliderButton, this, std::placeholders::_1));

		// set button colors
		m_decrease->SetBackgroundColor(m_btnDarkColor);
		m_decrease->SetForegroundColor(m_btnLitColor);
		m_increase->SetBackgroundColor(m_btnDarkColor);
		m_increase->SetForegroundColor(m_btnLitColor);
		m_slider->SetBackgroundColor(m_btnDarkColor);
		m_slider->SetForegroundColor(m_btnLitColor);
	}

	virtual ~UIScrollbar() {}


	virtual void	Render(SDL_Surface* screen) const override {
		UIWindow::Render(screen);		// draw background color
		
		// render buttons
		m_decrease->Render(screen);
		m_increase->Render(screen);

		m_slider->AdjustBounds(CalculateSliderBounds());
		m_slider->Render(screen);

		// this was removed (used for debug purposes)
		/*if (m_type == HORIZONTAL) {
			thickLineRGBA(screen, m_slider->GetBounds().x + m_slider->GetBounds().w / 2, m_slider->GetBounds().y + m_slider->GetBounds().h / 2, m_slider->GetBounds().x + m_slider->GetBounds().w / 2, m_slider->GetBounds().y - 100, 2, 255, 0, 0, 255);
		} else thickLineRGBA(screen, m_slider->GetBounds().x, m_slider->GetBounds().y + m_slider->GetBounds().h / 2, m_slider->GetBounds().x - 100, m_slider->GetBounds().y + m_slider->GetBounds().h / 2, 2, 255, 0, 0, 255);*/

			// border for entire window
			//Sint16	x[] = {m_bounds.x, m_bounds.x + m_bounds.w, m_bounds.x + m_bounds.w, m_bounds.x};
			//Sint16	y[] = {m_bounds.y, m_bounds.y, m_bounds.y + m_bounds.h, m_bounds.y + m_bounds.h};

			rectangleRGBA(screen, m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.w, m_bounds.y + m_bounds.h, 0, 0, 0, 255);

			// button borders
			rectangleRGBA(screen, m_decrease->GetBounds().x, m_decrease->GetBounds().y, m_decrease->GetBounds().x + m_decrease->GetBounds().w, m_decrease->GetBounds().y + m_decrease->GetBounds().h, 0, 0, 0, 255);
			rectangleRGBA(screen, m_increase->GetBounds().x, m_increase->GetBounds().y, m_increase->GetBounds().x + m_increase->GetBounds().w, m_increase->GetBounds().y + m_increase->GetBounds().h, 0, 0, 0, 255);
			rectangleRGBA(screen, m_slider->GetBounds().x, m_slider->GetBounds().y, m_slider->GetBounds().x + m_slider->GetBounds().w, m_slider->GetBounds().y + m_slider->GetBounds().h, 0, 0, 0, 255);

		// arrow polygons
		typedef std::array<Sint16, 3>		SdlGfxCoords;

		SdlGfxCoords		xCoords;
		SdlGfxCoords		yCoords;

		auto	insertionCoords = [&](const TriangleArray& src) {
			for (unsigned int i = 0; i < xCoords.size(); ++i) {
				xCoords[i] = static_cast<SdlGfxCoords::value_type>(src[i].x);
				yCoords[i] = static_cast<SdlGfxCoords::value_type>(src[i].y);
			}
		};

			// decrease polygon
			insertionCoords(m_decreaseTriangle);
			filledPolygonColor(screen, xCoords.data(), yCoords.data(), xCoords.size(), m_decrease->GetForegroundColor().GetGfxColor());
			
			// increase polygon
			insertionCoords(m_increaseTriangle);
			filledPolygonColor(screen, xCoords.data(), yCoords.data(), xCoords.size(), m_increase->GetForegroundColor().GetGfxColor());

	}


	virtual bool	HandleEvent(const SDL_Event& evt) override {
		if (UIWindow::HandleEvent(evt))
			return true;

		if (!m_slider->HandleEvent(evt) && !m_decrease->HandleEvent(evt) && !m_increase->HandleEvent(evt)) {
			// is the mouse on our window?
			if (IsPointWithinBounds(static_cast<int>(evt.motion.x), static_cast<int>(evt.motion.y), m_bounds)) {
				// yes, it is.  Left click?
				if (evt.type == SDL_MOUSEBUTTONDOWN)
					if (evt.button.button == SDL_BUTTON_LEFT) {
						// we must assume the click passed all 3 buttons.  Therefore, we need only figure out
						// which side of the slider it's on to determine the direction to step in
						const SDL_Rect	sliderPos = m_slider->GetBounds();
						bool	bDecreaseValue = false;

						if (IsHorizontal()) {
							if (evt.motion.x < (sliderPos.x + sliderPos.w / 2))
								bDecreaseValue = true;
						} else if (IsVertical()) {
							if (evt.motion.y < (sliderPos.y + sliderPos.h / 2))
								bDecreaseValue = true;
						}
					
						bDecreaseValue ? DecreaseButton(1) : IncreaseButton(1);

						return true;
					}
			} // fall through if not left click ...

			// check for mouse movement if the slider is pressed
			if (m_slider->GetState() == UIButton::PRESSED) {
				if (!m_sliderDragging) {
					// difference between the center of the slider button and the mouse position
					m_mouseSliderOffset.x = evt.motion.x - (m_slider->GetBounds().x + m_slider->GetBounds().w / 2);
					m_mouseSliderOffset.y = evt.motion.y - (m_slider->GetBounds().y + m_slider->GetBounds().h / 2);

					m_sliderDragging = true;
				}

				if (evt.type == SDL_MOUSEMOTION) {
					int	mousex = static_cast<int>(evt.motion.x) - m_mouseSliderOffset.x;
					int mousey = static_cast<int>(evt.motion.y) - m_mouseSliderOffset.y;

					if (IsHorizontal()) {
						// we want the slider to have its center at the mouse position
						int scrollRange = m_bounds.w - m_increase->GetBounds().w - m_decrease->GetBounds().w - m_slider->GetBounds().w;
						mousex = std::max<int>(0, std::min<int>(mousex - m_bounds.x - m_decrease->GetBounds().w - m_slider->GetBounds().w / 2, scrollRange));

						float percentage = (mousex) / static_cast<float>(scrollRange);
						percentage = std::max<float>(0.0f, std::min<float>(1.0f, percentage));
						m_value = m_valueMin + static_cast<ValueType>((m_valueMax - m_valueMin) * percentage);

						return true;
					} else {
						int scrollRange = m_bounds.h - m_increase->GetBounds().h - m_decrease->GetBounds().h - m_slider->GetBounds().h;
						mousey = std::max<int>(0, std::min<int>(mousey - m_bounds.y - m_decrease->GetBounds().h - m_slider->GetBounds().h / 2, scrollRange));

						float percentage = (mousey) / static_cast<float>(scrollRange);
						percentage = std::max<float>(0.0f, std::min<float>(1.0f, percentage));
						m_value = m_valueMin + static_cast<ValueType>((m_valueMax - m_valueMin) * percentage);

						return true;
					}

				} else if (evt.type == SDL_MOUSEBUTTONUP)		// If the user "unclicks" anywhere and we're lit, interpret that event
					if (evt.button.button == SDL_BUTTON_LEFT) {	// as an end to the click-drag
						m_slider->SetState(UIButton::NOT_PRESSED);
						m_sliderDragging = false;
						return true;
					}

			} // end (slider state == PRESSED) statement
		} else return true; // one of our button handled the event
			
		return false;
	}

	virtual void	DecreaseButton(int i) {
		if (i == UIButton::PRESSED) // pressed
			m_value = std::max<ValueType>(m_valueMin, m_value - m_bigStep);
	}

	virtual void	IncreaseButton(int i) {
		if (i == UIButton::PRESSED) // pressed
			m_value = std::min<ValueType>(m_valueMax, m_value + m_bigStep);
	}

	virtual void	SliderButton(int i) {
		UNREFERENCED_PARAMETER(i);
		// unfortunately this event will only occur when a PRESSED button
		// receives a MOUSEUP; so we need to detect when the button is 
		// first highlighted in HandleEvents() ourselves
		m_sliderDragging = false;
	}

	void	SetValue(ValueType newValue) {
		m_value = std::max<ValueType>(std::min<ValueType>(m_valueMin, newValue), m_valueMax);
	}

	ValueType	GetValue() const {
		return m_value;
	}

	bool	IsHorizontal() const {
		return HasStyle(UISTYLE::HORIZONTAL);
	}

	bool	IsVertical() const {
		return HasStyle(UISTYLE::VERTICAL);
	}

	UIButton&	GetDecreaseButton() { return *m_decrease; }
	UIButton&	GetIncreaseButton() { return *m_increase; }
	UIButton&	GetSliderButton() { return *m_slider; }
};

namespace UI {
	typedef UIScrollbar<int>							DefaultScrollbar;
	typedef UIScrollbar<int>							UIScrollbar_t;
	typedef std::shared_ptr<UIScrollbar_t>				BasicUIScrollbarPtr;
};

