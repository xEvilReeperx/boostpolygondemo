#pragma once
#include <SDL_stdinc.h>

namespace UI {
	enum UISTYLE : Uint32 {
		NONE				= (1 << 0),
		FANCY				= (1 << 1),
		TEXT_CENTERED		= (1 << 2),
		TEXT_ALIGN_LEFT		= (1 << 3),
		TEXT_ALIGN_RIGHT	= (1 << 4),
		HORIZONTAL			= (1 << 5),
		VERTICAL			= (1 << 6)
	};


}; // end UI namespace