#include <SDL_video.h>
#include <cassert>
#include <SDL_gfxPrimitives.h>
#include "PolygonTypes.h"
#include "PolygonGeometry.h"
#include "SDL_gfx_ext.h"

using PolygonTypes::Polygon;
using PolygonTypes::PolygonWh;
using PolygonTypes::PolygonVec;


/**************************************************************************//**
 *  Constructs the PolygonGeometry.  We require the drawing surface here so
 *	surfaces we'll be creating to render with later need to be optimized
 *	or there will be a huge speed penalty due to conversion.
 *
 *	\param drawSurface Surface most likely to be used as a destination for blitting
 *	\pre drawSurface was initialized already (and correctly) and a clipping rect
 *		 has been set up to tell us where we'll be allowed to blit stuff
 *****************************************************************************/
PolygonGeometry::PolygonGeometry(SDL_Surface* drawSurface) : m_workSurface(nullptr),
															 m_gridTexture(nullptr) {

	assert(drawSurface);

// create work texture
	int renderWidth = drawSurface->clip_rect.w - drawSurface->clip_rect.x;
	int renderHeight = drawSurface->clip_rect.h - drawSurface->clip_rect.y;

	assert(renderWidth > 0 && renderHeight > 0);

	SDL_Surface* unoptimized = SDL_CreateRGBSurface(SDL_HWSURFACE, renderWidth, renderHeight, 32, drawSurface->format->Rmask, drawSurface->format->Gmask, drawSurface->format->Bmask, drawSurface->format->Amask);
	if (!unoptimized) 
		throw std::exception("Failed to create work surface in PolygonGeometry.");
	

	m_workSurface = SDL_DisplayFormatAlpha(unoptimized);
	SDL_FreeSurface(unoptimized);

	// load texture
	SDL_Surface*	tex = SDL_LoadBMP("grid.bmp");
		if (!tex) {
			// try the parent dir, just in case the user forgot to move the image
			tex = SDL_LoadBMP("../grid.bmp");

			if (!tex)
				throw std::exception("Could not find grid.bmp texture.");
		}

	m_gridTexture = SDL_DisplayFormat(tex); SDL_FreeSurface(tex);

	assert(m_workSurface);
	assert(m_gridTexture);

	Reset();
}



/**************************************************************************//**
 *  Destructs geometry and frees surfaces.
 *****************************************************************************/
PolygonGeometry::~PolygonGeometry() {
	if (m_workSurface) SDL_FreeSurface(m_workSurface);
	if (m_gridTexture) SDL_FreeSurface(m_gridTexture);
}



/**************************************************************************//**
 *  Resets geometry to an empty "map" the size of the work surface (the
 *	clipped area we're allowed to draw in)
 *
 *	Note: Naturally this only resets our polygon geometry, if you want
 *	to reset everything you'll have to reset each geometry type.
 *
 *	\post Geometry is reset to a pristine state.
 *****************************************************************************/
void	PolygonGeometry::Reset() {
	m_geometry.self_.coords_.clear();
	m_geometry.holes_.clear();

	Polygon		grid; grid = boost::polygon::rectangle_data<int>(0, 0, m_workSurface->w, m_workSurface->h);

	// simple assignment
	boost::polygon::assign(m_geometry, grid);

	Polygon	hole; hole = boost::polygon::rectangle_data<int>(200, 200, 300, 300);
	AddHole(hole);
}



/**************************************************************************//**
 *  Render our polygon geometry.  It's expected that \a surface has its 
 *	clipping rect set no larger than our workSurface; otherwise an assert
 *	will occur.
 *
 *	\param surface Surface to blit on, which may have a clipping rect set
 *	\param bSegments Render polygon line segments
 *	\param bVertices Render polygon vertices
 *
 *	\post Geometry is rendered.
 *****************************************************************************/
void	PolygonGeometry::Render(SDL_Surface* surface, bool bSegments, bool bVertices) {
	// sanity check
	assert(surface->clip_rect.w - surface->clip_rect.x <= m_workSurface->w);
	assert(surface->clip_rect.h - surface->clip_rect.y <= m_workSurface->h);

	IntVector	xCoords;	
	IntVector	yCoords;

	//-------------------------------------------------------------------------
	//   Render the map polygon
	//-------------------------------------------------------------------------
	assert(!m_geometry.self_.coords_.empty());

	std::for_each(m_geometry.begin(), m_geometry.end(), [&xCoords, &yCoords](const PolygonWh::point_type& coords) {
		xCoords.push_back(static_cast<Sint16>(coords.x()));
		yCoords.push_back(static_cast<Sint16>(coords.y()));
	});

	// clear work texture ..
	SDL_FillRect(m_workSurface, NULL, SDL_MapRGBA(m_workSurface->format, 0, 255, 0, SDL_ALPHA_OPAQUE));

	// render the poly with the grid texture
	texturedPolygon(m_workSurface, &xCoords[0], &yCoords[0], static_cast<Uint8>(m_geometry.size()), m_gridTexture, 0, 0);

	if (bSegments) RenderLineSegments(m_workSurface, xCoords, yCoords, 0xFFDD11CC);
	if (bVertices) RenderVertices(m_workSurface, xCoords, yCoords, 0xFF0000CC, 3);

		// now we need to put some holes in the textured polygon ...
		for (auto hole = m_geometry.begin_holes(); hole != m_geometry.end_holes(); ++hole) {
			xCoords.clear();
			yCoords.clear();

			std::for_each(hole->begin(), hole->end(), [&xCoords, &yCoords](const PolygonWh::hole_type::point_type& pt) {
				xCoords.push_back(static_cast<Sint16>(pt.x()));
				yCoords.push_back(static_cast<Sint16>(pt.y()));
			});

			// "mask" this area of the workTexture so it doesn't get blitted (nothing's there, after all)
			//	 we hit a little snag here, though.  filledPolygonColor will blend SRC and DEST to produce
			//	 a final color.  That's not what we want.  We want to copy this color directly to the surface.
			//	 To do that, we'll borrow the scanline rendering implementation filledPolygon uses and make
			//	 a very slightly altered version ourselves...
			filledPolygonColorStore(m_workSurface, &xCoords[0], &yCoords[0], xCoords.size(), 0x000000FF);	// <-- note the 00 for A.  I've marked RR for red channel to make sure
																										// it's obvious if this failed

			RenderLineSegments(m_workSurface, xCoords, yCoords, 0xFFDD11CC);
			RenderVertices(m_workSurface, xCoords, yCoords, 0xFF0000CC, 3);
		}

	// blit results
		SDL_Rect	dest = {0,0,0,0};
		
		SDL_GetClipRect(surface, &dest);

	SDL_BlitSurface(m_workSurface, NULL, surface, &dest);
}



/**************************************************************************//**
 *  Adds a hole to the "map" geometry.
 *
 *	\pre poly is valid
 *	\post Geometry has poly's shape removed from it
 *
 *	\param poly Polygon representing hole in geometry.
 *****************************************************************************/
void	PolygonGeometry::AddHole(const Polygon& poly) {
	if (poly.size() == 0) return;

	// okay, we've got a little bit of a technical error if we just place the hole
	// wherever: it might overlap another hole.  If such a thing happens, let's combine
	// the two holes into one hole.  Boost::polygon makes this stupid easy
	PolygonVec		holes;		// okay, all our holes will be put in here
	
	// put in our first hole
		holes.push_back(poly);

	// okay, here we go!  iteratively put each of the old holes into our
	// vector, using boost::polygon to OR them together
	using namespace boost::polygon::operators;

		for (auto existingHoleIter = m_geometry.begin_holes(); existingHoleIter != m_geometry.end_holes(); ++existingHoleIter) {
			const Polygon&	existingHole = *existingHoleIter;

			// OR this poly with other holes
			boost::polygon::assign(holes, holes | existingHole);

			// see how easy that was?
		}

	// wipe out existing holes from the geometry, they're obsolete
	m_geometry.holes_.clear();

	// add new holes
	std::for_each(holes.begin(), holes.end(), [this](const Polygon& poly) {
		m_geometry.holes_.push_back(poly);

		// boost might make the last vertex == the first vertex in an OR operation, which will cause
		// an error in poly2tri
		if (m_geometry.holes_.back().coords_.back() == m_geometry.holes_.back().coords_.front())
			m_geometry.holes_.back().coords_.pop_back();
	});
}




