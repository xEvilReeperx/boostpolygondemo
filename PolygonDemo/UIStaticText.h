#pragma once
#include "UIButton.h"

/** Just static text, doesn't react to user **/
class UIStaticText : public UIButton {
public:
	UIStaticText(const std::string& label, const SDL_Rect& boundaries, Uint32 flags = UISTYLE::TEXT_CENTERED) 
		: UIButton(label, boundaries, UIButton::CallbackFunctionType(), flags) {
		SetBackgroundColor(GFX_INVISIBLE);
	}

	virtual bool	HandleEvent(const SDL_Event& evt) override { return UIWindow::HandleEvent(evt); }

	virtual void		Render(SDL_Surface* screen) const override {
		UIWindow::Render(screen);

		// clamp clipping rect to our bounds, so text doesn't
		// spill over

		Sint16	xString = m_bounds.x;
		Sint16	yString = m_bounds.y;

		if (HasStyle(UISTYLE::TEXT_CENTERED)) {
			xString = static_cast<Sint16>(m_bounds.x + m_bounds.w / 2 - m_label.size() / 2 * 8);
			yString = static_cast<Sint16>(m_bounds.y + m_bounds.h / 2 - 4);
		} else if (HasStyle(UISTYLE::TEXT_ALIGN_RIGHT)) {
			xString = static_cast<Sint16>(m_bounds.x + m_bounds.w - m_label.size() * 8);
		}
			
		RenderText(screen, m_label, m_foreColor, xString, yString);
	}
};