#pragma once
#include "UIButton.h"
#include <SDL_gfxPrimitives.h>

class UICheckbox : public UIButton {
protected:
	bool			m_checked;	

	virtual void Trigger(int arg) {
		(arg);

		// we assume the checkbox was just clicked, and therefore its
		// status needs to be toggled
		m_checked = !m_checked;

		// normally arg = 1 because this function is triggered
		// when the "button" of the checkbox is pressed.  However,
		// what we really want to send is the toggle state of the 
		// checkbox so we need to re-determine what to send here
		UIButton::Trigger(m_checked ? 1 : 0);
	}

public:
	UICheckbox(const std::string& lbl, const SDL_Rect& boundaries, CallbackFunctionType f, Uint32 flags = UISTYLE::NONE) : UIButton(lbl, boundaries, f, flags),
																							 m_checked(false) {
		assert(!m_label.empty());
	}

	virtual void	Render(SDL_Surface* screen) const override {
		SDL_Rect		rect = m_bounds;

		// small checkbox on the left
			// black rect
				rect.w = m_bounds.h;		// square rect

				rectangleColor(screen, rect.x, rect.y, rect.x + rect.w, rect.y + rect.h, 0x000000FF);

			// checkmark?
			if (m_checked) {
				// line from top-right to bottom-center
				thickLineColor(screen, rect.x + rect.w, rect.y, rect.x + rect.w / 2, rect.y + rect.h, 3, 0x00FF00FF);
				
				// line from bottom-center to middle-left
				thickLineColor(screen, rect.x + rect.w / 2, rect.y + rect.h, rect.x, rect.y + rect.h / 2, 2, 0x00FF00FF);
			}

		// text, just to the right of the checkbox rectangle
		rect.x = rect.x + rect.w + 5;
		
		RenderText(screen, m_label, GFX_BLACK, rect.x, m_bounds.y + m_bounds.h / 2 - 4);
	}

	virtual void	SetValue(bool bValue) { m_checked = !bValue; Trigger(0); }	// notice the inverted m_checked; that's because
																								// Trigger will invert it again assuming a button's been pressed
																				
																				
};