#pragma once

#include <boost/polygon/polygon.hpp>
#include <vector>
#include <SDL_types.h>
#include "poly2tri\poly2tri.h"

namespace PolygonTypes {
	typedef boost::polygon::polygon_data<int>					Polygon;
	typedef std::vector<Polygon>								PolygonVec;
	typedef boost::polygon::point_data<int>						PolygonPoint;
	typedef std::vector<PolygonPoint>							PointVec;
	typedef boost::polygon::polygon_with_holes_data<int>		PolygonWh;
	typedef std::vector<Sint16>									IntVector;
	typedef boost::polygon::polygon_set_data<PolygonWh>			PolygonSet;
	typedef std::pair<PolygonPoint, PolygonPoint>				PolygonSegment;
	typedef std::vector<PolygonSegment>							PolygonSegments;


	typedef std::vector<p2t::Triangle*>							P2TTriangleVector;
	typedef std::vector<p2t::Point*>							P2TPointVector;

}
