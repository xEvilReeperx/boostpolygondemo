#pragma once
#include <SDL_video.h>
#include <list>
#include <memory>
#include "UIWindow.h"


// note: sizer doesn't own any elements that get put into it
class Sizer {
protected:
	// local typedefs
	typedef std::weak_ptr<UIWindow>				WindowPtrWeak;
	typedef std::list<WindowPtrWeak>			WindowList;
	
	WindowList		m_contained;			///< controls contained by this sizer
	SDL_Rect		m_bounds;				///< area the sizer has to work with
	int				m_spacing;				

public:
	Sizer(const SDL_Rect& boundaries, int spacing = 0) : m_bounds(boundaries), m_spacing(spacing) {}
	Sizer() { m_bounds.x = m_bounds.y = m_bounds.w = m_bounds.h = 0; m_spacing = 0;}

	void	Add(WindowPtr& control) {
		if (control.use_count())
			m_contained.emplace_back(WindowPtrWeak(control));

		Update();
	}

	void	Clear() { m_contained.clear(); Update(); }

	virtual void	Update() = 0;			///< derived classes must implement their logic inside this function
	unsigned int	GetCount() const { return m_contained.size(); }
	SDL_Rect		GetBounds() const { return m_bounds; }
	void			SetBounds(const SDL_Rect& r) { m_bounds = r; Update(); }
};