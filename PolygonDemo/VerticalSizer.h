#pragma once
#include "Sizer.h"


// share vertical space equally.  Horizontal position untouched
class VerticalSizer : public Sizer {
public:
	VerticalSizer(const SDL_Rect& rect, int spacing = 0) : Sizer(rect, spacing) {}

	virtual void	Update() override {
		//if (GetCount() == 0)
		//	return;

		//Sint16	controlHeight = GetBounds().h / (GetCount() + 1);

		//// position all controls
		//SDL_Rect	controlRect = {GetBounds().x, GetBounds().y + controlHeight / 2, 0, 0};

		//for (auto& wpCtrl : m_contained) {
		//	WindowPtr	ptr = wpCtrl.lock();
		//	if (ptr) {
		//		SDL_Rect	ctrlRect = ptr->GetBounds();

		//		ctrlRect.y = controlRect.y;
		//		ptr->AdjustBounds(ctrlRect);

		//		// adjust rect for next control
		//		controlRect.y += controlHeight + controlHeight / GetCount();
		//	}
		//}
		unsigned int	controlCount = GetCount();
			if (controlCount == 0)
				return; // nothing to reposition

		// sanity checks
		assert(m_bounds.w > 0 && m_bounds.h > 0);		// invalid sizer dimensions

		Sint16	controlHeight = static_cast<Sint16>((m_bounds.h - (GetCount() - 2) * m_spacing) / (GetCount() + 1));
		Sint16	verticalPosition = static_cast<Sint16>(m_bounds.y + controlHeight);

		for (auto& weakptrControl : m_contained) {
			if (weakptrControl.expired()) continue;

			// get a strong reference to the control
			WindowPtr	ptr = weakptrControl.lock();

			// get the control's current rect
			SDL_Rect	childRect = ptr->GetBounds();

			// center the control on its new position
			childRect.y = verticalPosition - childRect.h / 2;

			// give the child its newp osition
			ptr->SetBounds(childRect);

			// update verticalPosition for the next control in sequence
			verticalPosition += static_cast<Sint16>(controlHeight + m_spacing);
		}
	}

};