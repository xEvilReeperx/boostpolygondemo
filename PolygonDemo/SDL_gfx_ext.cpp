#include <SDL_config.h>
#include <SDL_video.h>
#include <SDL_gfxPrimitives.h>
#include "SDL_gfx_ext.h"


// sorting function used by filledPolygonStore
int _gfxPrimitivesCompareInt(const void *a, const void *b) {
	return (*(const int *) a) - (*(const int *) b);
}



/**************************************************************************//**
 *  If the appropriate flag is set, draws a thick line between all vertices.
 *
 *	\pre dest is valid
 *
 *	\param dest SDL surface to draw on
 *	\param vx X coordinate vector
 *	\param vy Y coordinate vector
 *	\param color RGBA color (in SDL_Gfx style, i.e. ignore endianness)
 *****************************************************************************/
void	RenderLineSegments(SDL_Surface* dest, const IntVector& vx, const IntVector& vy, Uint32 color) {
	for (unsigned int vertex = 1; vertex < vx.size(); ++vertex)
		lineColor(dest, vx[vertex], vy[vertex], vx[vertex - 1], vy[vertex - 1], color);
		//thickLineColor(dest, vx[vertex], vy[vertex], vx[vertex - 1], vy[vertex - 1], 2, color);
		
	// connect last segment
	lineColor(dest, vx[0], vy[0], vx.back(), vy.back(), color);
	//thickLineColor(dest, vx[0], vy[0], vx.back(), vy.back(), 2, color);
}



/**************************************************************************//**
 *  If the appropriate flag is set, draws a small circle at each vertex
 *	location.
 *
 *	\pre dest is valid, radius is valid
 *
 *	\param dest SDL surface to draw on
 *	\param vx X coordinate vector
 *	\param vy Y coordinate vector
 *	\param color RGBA color (in SDL_Gfx style, i.e. ignore endianness)
 *	\param radius radius of circle in pixels
 *****************************************************************************/
void	RenderVertices(SDL_Surface* dest, const IntVector& vx, const IntVector& vy, Uint32 color, int radius) {
	if (dest)
		if (radius > 0)
			for (unsigned int i = 0; i < vx.size(); ++i)
				filledCircleColor(dest, vx[i], vy[i], static_cast<Sint16>(radius), color);
}


/*!
\brief Draw horizontal line without blending;

Just stores the color value (including the alpha component) without blending.
Only the same number of bits of the destination surface are transfered
from the input color value.

Note: copied without alteration from SDL_gfxPrimitives.h

\param dst The surface to draw on.
\param x1 X coordinate of the first point (i.e. left) of the line.
\param x2 X coordinate of the second point (i.e. right) of the line.
\param y Y coordinate of the points of the line.
\param color The color value of the line to draw. 

\returns Returns 0 on success, -1 on failure.
*/
int hlineColorStore(SDL_Surface * dst, Sint16 x1, Sint16 x2, Sint16 y, Uint32 color)
{
	Sint16 left, right, top, bottom;
	Uint8 *pixel, *pixellast;
	int dx;
	int pixx, pixy;
	Sint16 w;
	Sint16 xtmp;
	int result = -1;

	/*
	* Check visibility of clipping rectangle
	*/
	if ((dst->clip_rect.w==0) || (dst->clip_rect.h==0)) {
		return(0);
	}

	/*
	* Swap x1, x2 if required to ensure x1<=x2
	*/
	if (x1 > x2) {
		xtmp = x1;
		x1 = x2;
		x2 = xtmp;
	}

	/*
	* Get clipping boundary and
	* check visibility of hline 
	*/
	left = dst->clip_rect.x;
	if (x2<left) {
		return(0);
	}
	right = dst->clip_rect.x + dst->clip_rect.w - 1;
	if (x1>right) {
		return(0);
	}
	top = dst->clip_rect.y;
	bottom = dst->clip_rect.y + dst->clip_rect.h - 1;
	if ((y<top) || (y>bottom)) {
		return (0);
	}

	/*
	* Clip x 
	*/
	if (x1 < left) {
		x1 = left;
	}
	if (x2 > right) {
		x2 = right;
	}

	/*
	* Calculate width 
	*/
	w = x2 - x1;

	/*
	* Lock the surface 
	*/
	if (SDL_MUSTLOCK(dst)) {
		if (SDL_LockSurface(dst) < 0) {
			return (-1);
		}
	}

	/*
	* More variable setup 
	*/
	dx = w;
	pixx = dst->format->BytesPerPixel;
	pixy = dst->pitch;
	pixel = ((Uint8 *) dst->pixels) + pixx * (int) x1 + pixy * (int) y;

	/*
	* Draw 
	*/
	switch (dst->format->BytesPerPixel) {
	case 1:
		memset(pixel, color, dx+1);
		break;
	case 2:
		pixellast = pixel + dx + dx;
		for (; pixel <= pixellast; pixel += pixx) {
			*(Uint16 *) pixel = static_cast<Uint16>(color);
		}
		break;
	case 3:
		pixellast = pixel + dx + dx + dx;
		for (; pixel <= pixellast; pixel += pixx) {
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				pixel[0] = (color >> 16) & 0xff;
				pixel[1] = (color >> 8) & 0xff;
				pixel[2] = color & 0xff;
			} else {
				pixel[0] = color & 0xff;
				pixel[1] = (color >> 8) & 0xff;
				pixel[2] = (color >> 16) & 0xff;
			}
		}
		break;
	default:		/* case 4 */
		dx = dx + dx;
		pixellast = pixel + dx + dx;
		for (; pixel <= pixellast; pixel += pixx) {
			*(Uint32 *) pixel = color;
		}
		break;
	}

	/* 
	* Unlock surface 
	*/
	if (SDL_MUSTLOCK(dst)) {
		SDL_UnlockSurface(dst);
	}

	/*
	* Set result code 
	*/
	result = 0;

	return (result);
}


/*!
\brief Overwrites \a dst's pixel color with specified color (straight copy; no blending)

Just stores the color value (including the alpha component) without blending.
Only the same number of bits of the destination surface are transfered
from the input color value.

Note: copied with an extremely slight alteration from SDL_gfxPrimitives.h

\param dst The surface to draw on.
\param x1 X coordinate of the first point (i.e. left) of the line.
\param x2 X coordinate of the second point (i.e. right) of the line.
\param y Y coordinate of the points of the line.
\param color The color value of the line to draw. 

\returns Returns 0 on success, -1 on failure.
*/
int	filledPolygonColorStore(SDL_Surface* dst, const Sint16* vx, const Sint16* vy, int n, Uint32 color) {
	int result;
	int i;
	int y, xa, xb;
	int miny, maxy;
	int x1, y1;
	int x2, y2;
	int ind1, ind2;
	int ints;
	int *gfxPrimitivesPolyInts = NULL;
	int *gfxPrimitivesPolyIntsNew = NULL;
	int gfxPrimitivesPolyAllocated = 0;

	/*
	* Check visibility of clipping rectangle
	*/
	if ((dst->clip_rect.w==0) || (dst->clip_rect.h==0)) {
		return(0);
	}

	/*
	* Vertex array NULL check 
	*/
	if (vx == NULL) {
		return (-1);
	}
	if (vy == NULL) {
		return (-1);
	}

	/*
	* Sanity check number of edges
	*/
	if (n < 3) {
		return -1;
	}

	/*
	* Map polygon cache  
	*/
	//if ((polyInts==NULL) || (polyAllocated==NULL)) {
	//	/* Use global cache */
	//	gfxPrimitivesPolyInts = gfxPrimitivesPolyIntsGlobal;
	//	gfxPrimitivesPolyAllocated = gfxPrimitivesPolyAllocatedGlobal;
	//} else {
	//	/* Use local cache */
	//	gfxPrimitivesPolyInts = *polyInts;
	//	gfxPrimitivesPolyAllocated = *polyAllocated;
	//}

	/*
	* Allocate temp array, only grow array 
	*/
	if (!gfxPrimitivesPolyAllocated) {
		gfxPrimitivesPolyInts = (int *) malloc(sizeof(int) * n);
		gfxPrimitivesPolyAllocated = n;
	} else {
		if (gfxPrimitivesPolyAllocated < n) {
			gfxPrimitivesPolyIntsNew = (int *) realloc(gfxPrimitivesPolyInts, sizeof(int) * n);
			if (!gfxPrimitivesPolyIntsNew) {
				if (!gfxPrimitivesPolyInts) {
					free(gfxPrimitivesPolyInts);
					gfxPrimitivesPolyInts = NULL;
				}
				gfxPrimitivesPolyAllocated = 0;
			} else {
				gfxPrimitivesPolyInts = gfxPrimitivesPolyIntsNew;
				gfxPrimitivesPolyAllocated = n;
			}
		}
	}

	/*
	* Check temp array
	*/
	if (gfxPrimitivesPolyInts==NULL) {        
		gfxPrimitivesPolyAllocated = 0;
	}

	/*
	* Update cache variables
	*/
	//if ((polyInts==NULL) || (polyAllocated==NULL)) { 
	//	gfxPrimitivesPolyIntsGlobal =  gfxPrimitivesPolyInts;
	//	gfxPrimitivesPolyAllocatedGlobal = gfxPrimitivesPolyAllocated;
	//} else {
	//	*polyInts = gfxPrimitivesPolyInts;
	//	*polyAllocated = gfxPrimitivesPolyAllocated;
	//}

	/*
	* Check temp array again
	*/
	if (gfxPrimitivesPolyInts==NULL) {        
		return(-1);
	}

	/*
	* Determine Y maxima 
	*/
	miny = vy[0];
	maxy = vy[0];
	for (i = 1; (i < n); i++) {
		if (vy[i] < miny) {
			miny = vy[i];
		} else if (vy[i] > maxy) {
			maxy = vy[i];
		}
	}

	/*
	* Draw, scanning y 
	*/
	result = 0;
	for (y = miny; (y <= maxy); y++) {
		ints = 0;
		for (i = 0; (i < n); i++) {
			if (!i) {
				ind1 = n - 1;
				ind2 = 0;
			} else {
				ind1 = i - 1;
				ind2 = i;
			}
			y1 = vy[ind1];
			y2 = vy[ind2];
			if (y1 < y2) {
				x1 = vx[ind1];
				x2 = vx[ind2];
			} else if (y1 > y2) {
				y2 = vy[ind1];
				y1 = vy[ind2];
				x2 = vx[ind1];
				x1 = vx[ind2];
			} else {
				continue;
			}
			if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
				gfxPrimitivesPolyInts[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
			} 	    
		}

		qsort(gfxPrimitivesPolyInts, ints, sizeof(int), _gfxPrimitivesCompareInt);

		for (i = 0; (i < ints); i += 2) {
			xa = gfxPrimitivesPolyInts[i] + 1;
			xa = (xa >> 16) + ((xa & 32768) >> 15);
			xb = gfxPrimitivesPolyInts[i+1] - 1;
			xb = (xb >> 16) + ((xb & 32768) >> 15);
			result |= hlineColorStore(dst, static_cast<Sint16>(xa), static_cast<Sint16>(xb), static_cast<Sint16>(y), color);
		}
	}

	free(gfxPrimitivesPolyInts);

	return (result);
}