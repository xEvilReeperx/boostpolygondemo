#pragma once
#include <SDL_video.h>

/** Represents a color that can be shared between SDL and SDL_gfx.  Since 
	SDL_Gfx uses its own byte order scheme for colors, using a mapped
	color from SDL MAY NOT WORK.  Therefore this little helper class
	represents one color that we can convert to either type using its helper
	functions GetGfxColor() and GetMappedSDLColor().
**/
struct GfxColor {
	Uint8 r, g, b, a;

	// without explicit, it's actually possible to have the compiler break a Uint32 apart into components
	// and try to construct with that
	explicit GfxColor(Uint8 rr = 0, Uint8 gg = 0, Uint8 bb = 0, Uint8 aa = 255) : r(rr), g(gg), b(bb), a(aa) {}

	// use this color for SDL_Gfx* functions
	Uint32	GetGfxColor() const {
		Uint32	color = (r << 24) | (g << 16) | (b << 8) | a;
		return color;
	}

	// use this color for regular SDL functions (SDL_FillRect, etc)
	Uint32	GetMappedSDLColor(const SDL_Surface* const surf) const {
		if (a == 255)
			return SDL_MapRGB(surf->format, r, g, b);
		return SDL_MapRGBA(surf->format, r, g, b, a);
	}

	// convert SDL_gfx's RRGGBBAA format into a standard GfxColor
	static GfxColor PortableColorFromGfxColor(Uint32 gfxClr) {
		// simply break the color into its components
		Uint8 r = static_cast<Uint8>(gfxClr & 0xFF000000);
		Uint8 g = static_cast<Uint8>(gfxClr & 0x00FF0000);
		Uint8 b = static_cast<Uint8>(gfxClr & 0x0000FF00);
		Uint8 a = static_cast<Uint8>(gfxClr & 0x000000FF);

		return GfxColor(r, g, b, a);
	}

	// convert SDL's mapped format (which can vary!) into a standard GfxColor
	static GfxColor PortableColorFromSDLMappedColor(Uint32 sdlClr, const SDL_Surface* const surf) {
		// this one's a little trickier, because the colors might be swapped around
		Uint8 r = static_cast<Uint8>(sdlClr & surf->format->Rmask);
		Uint8 g = static_cast<Uint8>(sdlClr & surf->format->Gmask);
		Uint8 b = static_cast<Uint8>(sdlClr & surf->format->Bmask);
		Uint8 a = static_cast<Uint8>(sdlClr & surf->format->Amask);

		return GfxColor(r, g, b, a);
	}
};

inline GfxColor		CreateGfxColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a = 255) {
	return GfxColor(r, g, b, a);
}

const GfxColor	GFX_RED = GfxColor(255, 0, 0);
const GfxColor	GFX_BLACK = GfxColor(0, 0, 0);
const GfxColor	GFX_WHITE = GfxColor(255, 255, 255);
const GfxColor	GFX_LIGHT_GREY = GfxColor(192, 192, 192);
const GfxColor	GFX_DARK_GREY = GfxColor(141, 141, 141);
const GfxColor	GFX_INVISIBLE = GfxColor(255, 0, 255, 0);
