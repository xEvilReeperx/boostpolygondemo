#pragma once
#include "PolygonTypes.h"

/** Contains polygons which make up the map.  Non-traversable areas are 
	represented using polygon holes.
**/
class PolygonGeometry {
	PolygonTypes::PolygonWh			m_geometry;			///< The "map" if you will, which contains holes

	struct SDL_Surface*				m_gridTexture;		///< texture which we'll apply to polygons
	SDL_Surface*					m_workSurface;		///< an intermediate surface for rendering


public:
	void	Reset();							///< Clear all geometry data
	void	Render(SDL_Surface* surface, bool bSegments = false, bool bVertices = false);		///< Renders geometry in polygon form
	void	AddHole(const PolygonTypes::Polygon& poly);	///< Add a hole to the main geometry

	PolygonGeometry(SDL_Surface* drawSurface);			///< Init object.  Note that drawing area will be restricted to the clipping area of this surface
	~PolygonGeometry();
};