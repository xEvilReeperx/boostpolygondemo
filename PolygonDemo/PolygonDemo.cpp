/* LICENSE
 *
 * Copyright (c) 2013 xEvilReeperx
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// PolygonDemo.cpp : Defines the entry point for the console application.
//

#ifdef _DEBUG
#ifdef ENABLE_VLD
#include <vld.h>
#endif
#endif

#include <iostream>
#include <boost/filesystem.hpp>
#include "App.h"
#ifdef WIN32
#include <Windows.h>	// for MessageBox, Console
#endif

int main(int argc, char* argv[]) {
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	try {
		App					app;

		app.Go();
	} catch (int failCode) {
		std::cerr << "Caught fail code " << failCode << "; program is terminating." << std::endl;
		return failCode;
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
#ifdef WIN32
		MessageBoxW(0, TEXT("An error has occurred and the program must exit."), TEXT("Terminating"), MB_OK);
#endif
		return -1;
	} catch (...) {
		std::cerr << TEXT("Unknown terminal exception was thrown.") << std::endl;
#ifdef WIN32
		MessageBoxW(0, TEXT("An unknown exception has occurred."), TEXT("Terminating"), MB_OK);
#endif
		return -200;
	}

	return 0;
}

