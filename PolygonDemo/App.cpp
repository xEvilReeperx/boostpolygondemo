/* LICENSE
 *
 * Copyright (c) 2013 xEvilReeperx
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <SDL.h>
#include <SDL_events.h>
#include <SDL_gfxPrimitives.h>
#include "App.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include <fstream>
#include <exception>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include "UIScrollbar.h"
#include "UIStaticFrame.h"
#include "UIButton.h"
#include "UIOption.h"
#include "UICheckbox.h"
#include "VerticalSizer.h"

// user interface options
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 800;
const int UI_WIDTH = 200;			// width of ui panel on the right

const int CONTROL_HEIGHT = 20;		// height of controls in pixels
const int SCROLLBAR_SIZE = 16;		// height or width of the scrollbars (horizontal and vertical respectively)


// the dimensions of the "map" 
const int GEOMETRY_WIDTH = 2000;
const int GEOMETRY_HEIGHT = 2000;

// edit mode options
const int CIRCLE_SEGMENTS = 20;
const float CIRCLE_RADIUS = 75.0f;

// small helper function
using UIHelper::AsType;



int		App::Initialize() {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return -1;

	// create the drawing surface (and its backbuffer)
	if ((m_screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL) 
		return -2;
	

	// set name of window, no icon
	SDL_WM_SetCaption("Automatic Navigation Mesh Generation", nullptr);

	InitUI();
	InitGeometry();
	
	return 0; // success
}


// called when App goes out of scope in main.cpp
void	App::Unload() {
	// other elements are held together with shared_ptrs and will handle themselves when
	// they go out of scope

	SDL_Quit();
}


bool	App::HandleEvents() {
	// just watch for escape and exit when found
	SDL_Event		evt;

	while (SDL_PollEvent(&evt)) 
		if (evt.type == SDL_QUIT) {
			return false;
		} else if (evt.type == SDL_KEYDOWN) {
			if (evt.key.keysym.sym == SDLK_ESCAPE)
				return false;

			if (evt.key.keysym.sym == SDLK_PRINT) {
				// take a screenshot
				if (-1 == SDL_SaveBMP(m_screen, "ss.bmp")) {
#ifdef _WIN32
					MessageBoxW(0, TEXT("Failed to save screenshot."), TEXT("Error"), MB_OK | MB_ICONERROR);
#endif
				}
			} else if (evt.key.keysym.sym == SDLK_v) {
				m_bShowVertices = !m_bShowVertices;
			} else if (evt.key.keysym.sym == SDLK_s) {
				m_bShowSegments = !m_bShowSegments;

			}
		} else {
			// event wasn't one consumed by the mainloop; pass it to the root window
			// (the root window will pass it to all other ui elements)
			m_rootWindow->HandleEvent(evt); 
		}
	
	return true; // continue running
}


// constructor
App::App() : m_screen(nullptr), 

			 m_bShowSegments(true),
			 m_bShowVertices(true) {


	int	ret = Initialize();

	if (ret < 0)
		throw std::exception("Initialize() failed");

	//// create the first polygon which we'll be subtracting from.  
	//// assuming the grid covers the screen, just use the screen dimensions

	//Polygon		grid; grid = boost::polygon::rectangle_data<int>(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	//// simple assignment
	//boost::polygon::assign(m_geometry, grid);

	//// ***************************************************************************
	////   Cut out a somewhat complex polygon
	//// ***************************************************************************
	//Polygon		cutout;

	//struct coords { int x; int y; }; // can't seem to use initializer list with PolygonPoint, so a temporary definition will do

	//// here's the polygon we're going to subtract from our geometry
	//coords		coordinates[] = {
	//								{385, 192}, {416, 192}, {443, 200}, {451, 221}, 
	//								{452, 240}, {438, 256}, {425, 268}, {412, 273}, 
	//								{389, 279}, {365, 293}, {347, 307}, {337, 327},
	//								{338, 343}, {353, 360}, {376, 351}, {402, 345},
	//								{421, 339}, {450, 338}, {478, 345}, {491, 356},
	//								{520, 396}, {522, 406}, {521, 438}, {510, 458}, 
	//								{494, 468}, {476, 466}, {434, 445}, {399, 432}, 
	//								{382, 433}, {359, 455}, {355, 468}, {350, 494}, 
	//								{345, 528}, {331, 554}, {310, 556}, {290, 552}, 
	//								{264, 544}, {231, 526}, {215, 513}, {208, 498}, 
	//								{202, 478}, {197, 454}, {193, 430}, {194, 405},
	//								{195, 379}, {184, 358}, {169, 348}, {155, 341}, 
	//								{142, 319}, {143, 300}, {154, 281}, {171, 271}, 
	//								{211, 262}, {227, 262}, {241, 257}, {250, 238}, 
	//								{256, 213}, {267, 194}, {295, 205}, {297, 206}, 
	//								{328, 198}, {333, 170}, {356, 153}, {368, 157}, 
	//								{374, 165}, {378, 172}
	//							};

	//for (const coords& c : coordinates) 
	//	cutout.coords_.push_back(PolygonPoint(c.x, c.y));
	//
	//AddHole(cutout);

	//Triangulate();
}


// destructor
App::~App() {
	Unload();
}


// main loop of the program
void	App::Go() {
	// main loop
	while (HandleEvents()) {
		try {
			Render();
		} catch (const std::exception&) {
#ifdef WIN32
			int response = MessageBoxW(0, TEXT("An exception has occurred.  Do you want to continue running the program?"), TEXT("Exception"), MB_YESNO | MB_ICONQUESTION);

			if (response == IDNO) {
				// exit with a quit message
				SDL_Event quitEvt; quitEvt.type = SDL_QUIT;
				SDL_PushEvent(&quitEvt);		
				continue;
			}
#else
#error		Platform-specific message here?  Assuming the exception isn't recoverable ...
				loop = false;
				continue;
#endif
		}
    };
}



/**************************************************************************//**
 *  Create a UserInterface object, add user interface elements to it and 
 *	set up callbacks.
 *
 *	\post User Interface is set up (throws exception on failure)
 *****************************************************************************/
void	App::InitUI() {

	// create (invisible) root window
	SDL_Rect	rootRect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
	auto defaultFn = std::bind(&App::DoNothing, this, std::placeholders::_1);

	m_rootWindow = std::make_shared<WindowPtr::element_type>(rootRect, GfxColor(255, 0, 255, 0));

		// create (invisible) render window
		SDL_Rect	rwRect = {0, 0, SCREEN_WIDTH - UI_WIDTH - SCROLLBAR_SIZE, SCREEN_HEIGHT - SCROLLBAR_SIZE - 1};
		m_renderWindow = std::make_shared<WindowPtr::element_type>(rwRect, GfxColor(255, 0, 255));
		m_rootWindow->AddChild(m_renderWindow);


		// create right panel
		SDL_Rect	panelRect = {SCREEN_WIDTH - UI_WIDTH, 0, UI_WIDTH, SCREEN_HEIGHT};
		WindowPtr	rightPanel = std::make_shared<WindowPtr::element_type>(panelRect, GFX_LIGHT_GREY);


		// right panel divided into 3 parts: edit mode, geometry options, layer options
		SDL_Rect	editModeSection = {SCREEN_WIDTH - UI_WIDTH + 2, panelRect.y + 8, panelRect.w - 4, static_cast<Sint16>(panelRect.h * 0.16f)}; // 16% to edit mode
		SDL_Rect	geomOptSection = {editModeSection.x, editModeSection.y + editModeSection.h + 8, editModeSection.w, static_cast<Sint16>(panelRect.h * 0.15f)};
		SDL_Rect	layerSection = {geomOptSection.x, geomOptSection.y + geomOptSection.h + 8, geomOptSection.w, static_cast<Sint16>(panelRect.h * 0.45f)}; // 45%
		GfxColor	panelColor(127, 127, 127);
		SDL_Rect	controlRect = {editModeSection.x + 3, editModeSection.y, editModeSection.w - 4, CONTROL_HEIGHT};

			// ------------------------------------------------
			//		Edit mode panel
			// ------------------------------------------------
			WindowPtr		emPanel = std::make_shared<UIStaticFrame>("Edit mode", editModeSection, panelColor, UISTYLE::FANCY);
			emPanel->SetStyle(UISTYLE::FANCY);
			VerticalSizer emSizer(emPanel->GetBounds(), 4);

			{
				// ###############################################
				//			circle option button
				// ###############################################
				WindowPtr		optCircle = std::make_shared<UIOption>("Circle", controlRect, defaultFn);
				optCircle->SetBackgroundColor(GfxColor(0, 0, 0));
				optCircle->SetForegroundColor(GfxColor(0, 0, 0));
				AsType<UIOption>(optCircle)->SetValue(true);

				emSizer.Add(optCircle);
				emPanel->AddChild(optCircle);


				// ###############################################
				//			vertex option button
				// ###############################################
				WindowPtr		optVertex = std::make_shared<UIOption>("Vertex", controlRect, defaultFn, AsType<UIOption>(optCircle)->GetOptionGroup());
				optVertex->SetBackgroundColor(GfxColor(0, 0, 0));
				optVertex->SetForegroundColor(GfxColor(0, 0, 0));
				optVertex->SetStyle(UISTYLE::FANCY);

				emSizer.Add(optVertex);
				emPanel->AddChild(optVertex);


				// ###############################################
				//			undo button
				// ###############################################
				WindowPtr	undoButton = std::make_shared<UIButton>("Undo", controlRect, defaultFn, UISTYLE::FANCY);

				emSizer.Add(undoButton);
				emPanel->AddChild(undoButton);


				// ###############################################
				//			redo button
				// ###############################################
				WindowPtr	redoButton = std::make_shared<UIButton>("Redo", controlRect, defaultFn, UISTYLE::FANCY);

				emSizer.Add(redoButton);
				emPanel->AddChild(redoButton);


				// ###############################################
				//			reset button
				// ###############################################
				WindowPtr	resetButton = std::make_shared<UIButton>("Reset", controlRect, defaultFn, UISTYLE::FANCY);

				emSizer.Add(resetButton);
				emPanel->AddChild(resetButton);
			} // end edit mode panel controls


			// ------------------------------------------------
			//		geometry options panel
			// ------------------------------------------------
			WindowPtr		geomPanel = std::make_shared<UIStaticFrame>("Geometry Options", geomOptSection, panelColor, UISTYLE::FANCY);
			VerticalSizer	geomSizer(geomPanel->GetBounds());

			{
				// ###############################################
				//			check display vertices
				// ###############################################
				WindowPtr	chkVertices = std::make_shared<UICheckbox>("Display vertices", controlRect, defaultFn);
				AsType<UICheckbox>(chkVertices)->SetForegroundColor(GfxColor(0, 255, 0, 128));

				geomSizer.Add(chkVertices);
				geomPanel->AddChild(chkVertices);


				// ###############################################
				//			check display line segments
				// ###############################################
				WindowPtr	chkSegments = std::make_shared<UICheckbox>("Display line segments", controlRect, defaultFn);
				AsType<UICheckbox>(chkSegments)->SetForegroundColor(GfxColor(0, 255, 0, 128));

				geomSizer.Add(chkSegments);
				geomPanel->AddChild(chkSegments);


				// ###############################################
				//			check display colored polygons
				// ###############################################
				WindowPtr	chkColoredPolys = std::make_shared<UICheckbox>("Display colored polygons", controlRect, defaultFn);
				AsType<UICheckbox>(chkColoredPolys)->SetForegroundColor(GfxColor(0, 255, 0, 128));

				geomSizer.Add(chkColoredPolys);
				geomPanel->AddChild(chkColoredPolys);


			} // end geom panel buttons
			// ------------------------------------------------
			//		layer panel
			// ------------------------------------------------
			WindowPtr		layerPanel = std::make_shared<UIStaticFrame>("Layer Selection", layerSection, panelColor, UISTYLE::FANCY);


		// add the three section panels to the right panel
		rightPanel->AddChild(emPanel);
		rightPanel->AddChild(geomPanel);
		rightPanel->AddChild(layerPanel);

		// connect right panel to the invisible root window
		m_rootWindow->AddChild(rightPanel);

		// ------------------------------------------------
		//		Render window scrollbars
		// ------------------------------------------------
		
			// ###############################################
			//			vertical scrollbar for render window
			// ###############################################
			SDL_Rect	vsRect = {m_renderWindow->GetBounds().x + m_renderWindow->GetBounds().w, 0, SCROLLBAR_SIZE, m_renderWindow->GetBounds().h + SCROLLBAR_SIZE};

			m_renderVScroll = BasicUIScrollbarPtr(new UIScrollbar_t(vsRect, UISTYLE::VERTICAL, GFX_LIGHT_GREY, GFX_DARK_GREY, GfxColor(59, 59, 59), 0, GEOMETRY_HEIGHT, 0, GEOMETRY_HEIGHT / 20, m_renderWindow->GetBounds().h));
			m_rootWindow->AddChild(m_renderVScroll);

			// ###############################################
			//			horizontal scrollbar for render window
			// ###############################################
			SDL_Rect	hsRect = {m_renderWindow->GetBounds().x, m_renderWindow->GetBounds().x + m_renderWindow->GetBounds().h, m_renderWindow->GetBounds().w, SCROLLBAR_SIZE};

			m_renderHScroll = BasicUIScrollbarPtr(new UIScrollbar_t(hsRect, UISTYLE::HORIZONTAL, GFX_LIGHT_GREY, GFX_DARK_GREY, GfxColor(59, 59, 59), 0, GEOMETRY_WIDTH, 0, GEOMETRY_WIDTH / 20, m_renderWindow->GetBounds().w));
			m_rootWindow->AddChild(m_renderHScroll);

}



/**************************************************************************//**
 *  Create all data models and prepare them with initial data.
 *
 *	\post Data models are ready to use
 *****************************************************************************/
void	App::InitGeometry() {
	// the various geometry models may require some offscreen surfaces for
	// efficient rendering; in those cases, the constructor expects a pointer
	// to the surface it will generally be rendering to with a clipping rect
	// specifying the area it's allowed to use

	m_polygonGeometry = std::make_shared<PolygonGeometryPtr::element_type>(m_screen /* with clip rect attached */);

}



/**************************************************************************//**
 *  A stub function which redirects rendering to either "polygon" or
 *	"triangulation" mode.
 *
 *	\pre drawSurface is valid
 *	\post drawSurface has been used for rendering
 *
 *	\param drawSurface Which surface to blit results to
 *****************************************************************************/
void	App::Render() {
	SDL_SetClipRect(m_screen, nullptr);
	SDL_FillRect(m_screen, nullptr, SDL_MapRGB(m_screen->format, 255, 0, 255));


		//-----------------------------
		//	Geometry
		//-----------------------------
			// TODO: determine which geometry to render here
			m_polygonGeometry->Render(m_screen, m_bShowSegments, m_bShowVertices);

		//-----------------------------
		//	UI
		//-----------------------------
			SDL_SetClipRect(m_screen, nullptr);
			m_rootWindow->Render(m_screen);


	SDL_Flip(m_screen);
}



/**************************************************************************//**
 *  Take a screenshot and save to working directory.
 *
 *	\post Screenshot bitmap is taken, or an error message box appears.
 *****************************************************************************/
void	App::Screenshot() {
	using boost::filesystem::path;
	using boost::filesystem::exists;

	// ensure a screenshots directory exists in our working folder
	if (!exists(path("screenshots/"))) {
		// create a screenshots dir
		boost::filesystem::create_directory(path("./screenshots/"));
	}

	// find an appropriate file name for this screenshot
	std::string		baseFilename = "./screenshots/screenshot_";
	std::string		strNumber;
	std::string		ext(".bmp");
	path			pFilename;

	unsigned int counter = 0;

	do {
		strNumber = boost::lexical_cast<std::string, unsigned int>(counter);

		// pad strNumber out to 3 spaces; files in the directory will sort
		// nicer this way
		if (strNumber.length() < 3)
			strNumber.insert(strNumber.begin(), 3 - strNumber.length(), '0');

		pFilename = baseFilename + strNumber + ext;
	} while (!exists(pFilename));

	const std::string		fullPath(pFilename.string());

	if (-1 == SDL_SaveBMP(m_screen, fullPath.c_str())) {
#ifdef _WIN32
			MessageBoxW(0, TEXT("Failed to save screenshot."), TEXT("Error"), MB_OK | MB_ICONERROR);
#else
			std::cerr << "Failed to save screenshot.\n";
#endif
	}
}



//bool	App::EventCallback(const SDL_Event& evt) {
//	// PRINTSCREEN -> TakeScreenshot
//	// S -> toggle segments
//	// V -> toggle vertices
//
//	if (evt.key.keysym.sym == SDLK_PRINT) {
//		// take a screenshot
//		Screenshot();
//	} else if (evt.key.keysym.sym == SDLK_v) {              
//		m_bShowVertices = !m_bShowVertices;		// toggle vertices
//		return true;
//	} else if (evt.key.keysym.sym == SDLK_s) {				
//		m_bShowSegments = !m_bShowVertices;			// toggle segments
//		return true;
//	}
//
//	return false;
//}





/**************************************************************************//**
 *  Renders triangulated geometry (plus vertices and lines, if toggles have been
 *	enabled).
 *
 *	\param surface Where to render to.
 *****************************************************************************/
//void	App::RenderTriangulatedGeometry(SDL_Surface* surface) {
//	IntVector	xCoords;
//	IntVector	yCoords;
//
//	P2TTriangleVector	triangulation = m_cdt->GetTriangles();
//
//	// simply render every triangle in our triangulation
//	for (p2t::Triangle* tri : triangulation) {
//		xCoords.clear();
//		yCoords.clear();
//
//		for (int i = 0; i < 3; ++i) {
//			xCoords.push_back(static_cast<int>(tri->GetPoint(i)->x));
//			yCoords.push_back(static_cast<int>(tri->GetPoint(i)->y));
//		}
//
//		texturedPolygon(surface, &xCoords[0], &yCoords[0], 3, m_gridTexture, 0, 0);
//	}
//
//	// do a second pass for lines and vertices so newly drawn triangles
//	// don't overlap them
//	if (m_bShowSegments || m_bShowVertices)
//		for (p2t::Triangle* tri : triangulation) {
//			xCoords.clear();
//			yCoords.clear();
//			
//			for (int i = 0; i < 3; ++i) {
//				xCoords.push_back(static_cast<int>(tri->GetPoint(i)->x));
//				yCoords.push_back(static_cast<int>(tri->GetPoint(i)->y));
//			}
//
//			if (m_bShowSegments)
//				RenderLineSegments(surface, xCoords, yCoords, 0xFF8888DD);
//
//			if (m_bShowVertices)
//				RenderVertices(surface, xCoords, yCoords, 0x000000FF, 3);
//		}
//
//}






/**************************************************************************//**
 *  Uses triangle to perform a delaunay triangulation of what we'll call
 *	the "world geometry" -- a 'primary' polygon with holes in it.
 *
 *	\pre dest is valid
 *
 *	\param dest SDL surface to draw on
 *	\param vx X coordinate vector
 *	\param vy Y coordinate vector
 *	\param color RGBA color (in SDL_Gfx style, i.e. ignore endianness)
 *****************************************************************************/
//void	App::Triangulate() {
//	using p2t::Point;
//
//	// we have to use a naked pointer here sadly, poly2tri's rules
//	typedef std::vector<P2TPointVector>	HoleVector;
//
//	P2TPointVector							outerVertices; 
//	HoleVector								holeVertices; holeVertices.resize(m_geometry.holes_.size());
//
//	std::for_each(m_geometry.begin(), m_geometry.end(), [&outerVertices](const PolygonPoint& pt) {
//		outerVertices.emplace_back(new Point(static_cast<float>(pt.x()), static_cast<float>(pt.y())));
//	});
//
//		// alright, construct the triangulation object
//		m_cdt.reset();
//		m_cdt = std::shared_ptr<p2t::CDT>(new p2t::CDT(outerVertices)); // note: old triangulation will go out of scope and be destroyed
//		
//		// now add holes to that initial cdt
//		unsigned int holeIndex = 0;
//
//		for (auto holePolyIter = m_geometry.begin_holes(); holePolyIter != m_geometry.end_holes(); ++holePolyIter, ++holeIndex)
//			std::for_each(holePolyIter->begin(), holePolyIter->end(), [&holeIndex, &holeVertices](const PolygonPoint& pt) {
//				holeVertices.at(holeIndex).emplace_back(new Point(static_cast<float>(pt.x()), static_cast<float>(pt.y())));
//			});
//		
//		// insert each hole vertex vector into the cdt ...
//		for (const auto& hole : holeVertices)
//			m_cdt->AddHole(hole);
//		
//		// Triangulation time!
//		// -------------------------------
//			m_cdt->Triangulate();
//		// -------------------------------
//
//		// all this allocated memory will be stored in our m_triangulation member, so
//		// let's clear these vertices now so nobody touches them
//		holeVertices.clear();
//		outerVertices.clear();
//
//		//TriangleVector	delaunayTriangles = cdt.GetTriangles();
//		//
//		//// replace our old triangulation with the new one ..
//		//std::swap(m_triangulation, delaunayTriangles);
//
//		// and now release memory from the old triangulation
//		//for (auto triPtr : delaunayTriangles) {
//		//	for (int i = 0; i < 3; ++i)
//		//		delete triPtr->GetPoint(i);
//
//		//	delete triPtr;
//		//}
//
//		//delaunayTriangles.clear();
//}




